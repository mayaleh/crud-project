<?php
/**
 * Created by PhpStorm.
 * User: Mayaleh
 * Date: 2. 1. 2018
 * Time: 11:26
 */

namespace app\model;

use Nette\Database\Connection;

class Model
{
    protected $database;


    /**
     * Konstruktor vytvoří vrstvu databáze Explorer
     *@param  Connection $database předá se Nette\Database\Context
     */
    public function __construct(Connection $database)
    {
        $this->database = $database;
    }

}