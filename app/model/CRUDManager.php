<?php
/**
 * Created by PhpStorm.
 * User: Salim
 * Date: 30. 12. 2017
 * Time: 19:19
 */

namespace App\Model;

use Nette\Database\Connection;
use Nette\UnexpectedValueException;
use Nette\Utils\ArrayHash;

/**
 * Model operací CRUD projektů
 * @package App/Model
 */
class CRUDManager
{
    private $database;
    private $honotyVypis = array();
    private $values;

    private $dataAllOut = array();


    /**
     * Konstruktor vytvoří vrstvu databáze Explorer
     * @param  Connection $database předá se Nette\Database\Context
     */
    public function __construct(Connection $database)
    {
        $this->database = $database;
    }


    /**
     * Funkce detekuje požadavek (EDIT || ADD) a zavolá vhodnou operaci
     * @param ArrayHash $values - hodnoty předané presenterem z formuláře
     * @return $result
     */
    public function pozadavekCRUD($values)
    {

        $this->values = $this->nastaveniHodnot($values);


        if (isset($this->values['edit']) && $this->values['edit'] !== "") {
            $result = $this->editExistProject();
        } else {
            $result = $this->addNewProject();
        }
        return $result;
    }


    /**
     * Funkce nastavuje hodnoty pro Insert a pro výpis
     * @param $val
     * @return array
     */
    private function nastaveniHodnot($val)
    {
        if (!isset($val['id'])) {
            $val['id'] = false;
        } else {
            $this->honotyVypis['name'] = $val['name'];
            if (isset($val['id'])) {
                $this->honotyVypis['id'] = $val['id'];
            }

            if (isset($val['edit'])) {
                $this->values['edit'] = $val['edit'];
            }


            if ($val['web_project'] == true) {
                $val['web_project'] = 1;
                $this->honotyVypis['web_project'] = "Ano";
            } else {
                $val['web_project'] = 0;
                $this->honotyVypis['web_project'] = "Ne";
            }

            switch ($val['typ_project']) {
                case 1:
                    $this->honotyVypis['typ_project'] = "Časově omezený projekt";
                    break;
                case 2:
                    $this->honotyVypis['typ_project'] = "Dlouhodobý projekt";
                    break;
                default:
                    $this->honotyVypis['typ_project'] = "NULL";
                    break;
            }

            $this->honotyVypis['date'] = $val['date'];
            $val['date'] = date('Y-m-d', strtotime($val['date']));
        }


        return $val;
    }


    /**
     * Funkce pro výpis požadovaného projektu podle ID
     * @param $id - id projektu pro výpis
     * @return array project values
     */
    public function getProject($id)
    {
        return $this->nastaveniHodnot($this->database->fetch(
            'SELECT * FROM mayaleh_projects WHERE id = ? LIMIT 1',
            $id
        ));
    }

    /**
     * Funkce pro výpis všech projektů a nastavení pro výpis
     * @return array all projects
     */
    public function getAllProjects()
    {
        $data = $this->database->fetchAll(
            "SELECT
                    id,
                    name,
                    DATE_FORMAT(date, '%d. %m. %Y') as date,
                    typ_project,
                    web_project
                    FROM mayaleh_projects"
        );
        foreach ($data as $value) {
            $this->nastaveniHodnot($value);
            $this->dataAllOut[] = $this->honotyVypis;
        }
        return $this->dataAllOut;
    }


    /**
     * Funkce pro přidání nového projektu
     * @return null
     */
    private function addNewProject()
    {
        return $this->database->query("INSERT  INTO mayaleh_projects ?", $this->values);
    }

    /**
     * Funkce pro upraví existující projekt
     * @return null
     */
    private function editExistProject()
    {
        return $this->database->query(
            "UPDATE mayaleh_projects SET",
            [
                'name' => $this->values['name'],
                'date' => $this->values['date'],
                'typ_project' => $this->values['typ_project'],
                'web_project' => $this->values['web_project'],
            ],
            "WHERE id = ?",
            $this->values['edit']
        );
    }

    /**
     * Funkce pro vymaže projekt
     * @param  $id int - Id projektu
     * @return null
     */
    public function deleteProject($id)
    {
        return $this->database->query("DELETE FROM mayaleh_projects WHERE id=?", $id);
    }


    /**
     * Funkce pro návrat hodnoty pro čtení
     * @param $var - název proměnné
     * @return - promennou s nazvem $var, pokud existuje
     */
    public function __get($var)
    {
        if (property_exists($this, $var)) {
            return $this->$var;
        } else {
            throw new UnexpectedValueException('Unknown property name: ' . $var);
        }
    }
}
