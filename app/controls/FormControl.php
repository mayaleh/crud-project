<?php
/**
 * Created by PhpStorm.
 * User: Salim
 * Date: 15. 1. 2018
 * Time: 19:00
 */

namespace App\Controls;

use Nette\Application\UI;

class FormControl extends UI\Control
{
    /** Definice konstant pro zprávy formuláře. */
    const
        FORM_MSG_REQUIRED = 'Povinné pole!',
        FORM_MSG_RULE = 'Neplatný vstup.',
        FORM_MSG_SUCCES = 'Záznam byl uložen',
        FORM_MSG_EDITING = 'Prováníte úpravy projektu: ';

    private $data;

    /**
     * Vykreslování formuláře
     * @param $data array - pro předvyplnění formuláře
     * @return UI\Form $form
     */
    public function create($data = [])
    {
        $this->data = $data;
        $form = new UI\Form;

        $form->addText('name', 'Název projektu:')->setRequired(self::FORM_MSG_REQUIRED);
        $form->addText('date', 'Datum odevzdání projektu:')->setRequired(self::FORM_MSG_REQUIRED)->setType('date');
        $form->addCheckbox('web_project', 'Webový projekt:')->setRequired(false);
        $typProjektu = [
            '1' => 'Časově omezený projekt',
            '2' => 'Dlouhodobý projekt',
        ];
        // pro vypsání možností do 1 řádku
        $form->addRadioList('typ_project', 'Typ projektu:', $typProjektu)
            ->getSeparatorPrototype();
        $form->addSubmit('save', 'Uložit');

        //Nastaveni hodnot
        if (isset($this->data['edit']) && $this->data['edit'] > 0) {
            //nastaví hodnoty formuláře
            $form->setDefaults($this->data);
            $form->addHidden('edit', $this->data['edit']);


            //NEFUNGUJE... pres flashmessage
            //$this->templateData['editing'] = self::FORM_MSG_EDITING;
            //$this->flashMessage(self::FORM_MSG_EDITING.$this->data['name']);
        }
        $form->onSuccess[] = [$this, 'processForm'];
        return $form;
    }

    public function processForm(UI\Form $form, $values)
    {
        // mohu použít $this->database
        // zpracovani formulare
    }
}
