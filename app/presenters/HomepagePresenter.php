<?php

namespace App\Presenters;

use App\Model\CRUDManager;
use App\Controls\FormControl;
use Nette\Application\UI;

class HomepagePresenter extends UI\Presenter
{
    /** Definice konstant pro zprávy formuláře. */
    const
        FORM_MSG_REQUIRED = 'Povinné pole!',
        FORM_MSG_RULE = 'Neplatný vstup.',
        FORM_MSG_SUCCES = 'Záznam byl uložen',
        FORM_MSG_DELETE = 'Záznam byl vymazán.',
        FORM_MSG_EDITING = 'Upravujete projekt: ';


    /** @var CRUDManager Instance třídy modelu pro práci s daty. */
    private $crudManager;

    /** @var int Id editovaného projektu */
    private $editID;

    /** @var int výsledek poslaného požadavku na model */
    private $result;

    /** @var array vypsaní záznamu pro formulář pro editaci */
    public $zaznamEdit;


    /** @var \app\controls\FormControl @inject */
    public $formControlFactory;


    /**
     * Konsturktor Funkce s injektovaným modelem pro práci s daty.
     * @param CRUDManager $crudManager automaticky injektovaná třída modelu pro práci s daty
     */
    public function __construct(CRUDManager $crudManager)
    {
        parent::__construct();
        $this->crudManager = $crudManager;
    }


    /**
     * vykreslení do šablony Default
     */
    public function renderDefault()
    {
        $this->template->projecsOut = $this->crudManager->getAllProjects();
    }


    /**
     * akce Edit - získá data záznamu přes Model, pokud existuje v DB
     */
    public function actionEdit()
    {
        if (isset($this->request->getParameters()['id'])) {
            $this->editID = $this->request->getParameters()['id'];
            $this->zaznamEdit = $this->crudManager->getProject($this->editID);
            if ($this->zaznamEdit['id'] === false) {
                $this->flashMessage("Záznam neexistuje!", "error");
                $this->redirect('default');
            } else {
                $this->zaznamEdit['edit'] = $this->zaznamEdit['id'];
                $this->flashMessage(self::FORM_MSG_EDITING . $this->zaznamEdit['name']);
            }
        }
    }

    /**
     * vykreslení do šablony Edit
     */
    public function renderEdit()
    {
        $this->template->projecsOut = $this->crudManager->getAllProjects();
    }


    /**
     * akce Delete - vymaže záznam, pokud existuje
     */
    public function actionDelete()
    {
        if (isset($this->request->getParameters()['id'])) { // pokud existuje projekt tak ho vymaz
            $delete = $this->request->getParameters()['id'];
            $zaznam = $this->crudManager->getProject($delete);
            if ($zaznam['id'] === false) {
                $this->flashMessage("Záznam neexistuje!", "error");
            } else {
                $this->crudManager->deleteProject($zaznam['id']);
                $this->flashMessage(self::FORM_MSG_DELETE);
            }
        }
    }

    /**
     * vykreslení do šablony Delete - Přesměrování na default
     */
    public function renderDelete()
    {
        $this->redirect('default');
    }


    /**
     * Vrátí formulář pro přidání/editace projketu.
     * @return UI\Form formulář přidání/editace projketu
     */

    protected function createComponentMujForm()
    {
        if (isset($this->zaznamEdit)) {
            $form = $this->formControlFactory->create($this->zaznamEdit);
        } else {
            $form = $this->formControlFactory->create();
        }


        /**
         * Funkce se vykonaná při úspěšném odeslání formuláře přidání/editace projketu a
         * zpracuje odeslané hodnoty a nastaví
         * @param UI\Form $form object
         * @param $values array
         * hlášku o stavu odeslání formuláře.
         * @return object
         */
        $form->onSuccess[] = function (UI\Form $form, $values) {
            $httpRequest = $this->getHttpRequest();
            if (null !== $httpRequest->getPost("edit")) {
                $values['edit'] = $httpRequest->getPost("edit");
            }
            $this->result = $this->crudManager->pozadavekCRUD($values);
            $this->flashMessage(self::FORM_MSG_SUCCES);
            $form->reset();
        };
        return $form;
    }
}
