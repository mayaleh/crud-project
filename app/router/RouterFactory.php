<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');


//        $router[] = new Route('<presenter>/<action>[/<req>][/<id>]', [
//            'presenter' => 'Homepage',
//            'action' => 'default',
//            'req' => null,
//            'id' => null,
//        ]);
//
//        $router = new Route('<presenter>/<action>[/<req>][/<id>]', [
//            'presenter' => [
//                Route::VALUE => 'Homepage',
//            ],
//            'action' => [
//                Route::VALUE => 'default',
//            ],
//            'req' => [
//                Route::VALUE => null,
//            ],
//            'id' => [
//                Route::VALUE => null,
//            ],
//        ]);


		return $router;
	}
}
