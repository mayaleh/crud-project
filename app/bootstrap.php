<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode('23.75.345.200'); // nastavení "Vývojářský režim" pro IP adresu. Na localhost je zapnutý auto.

// zapnutí "Debugovací nástroj Tracy", chyby necháme logovat do uvedeného adresáře
$configurator->enableTracy(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');

// Cachování:
$configurator->setTempDirectory(__DIR__ . '/../temp');

// Zpravidla budeme chtít automaticky načítat třídy pomocí RobotLoaderu, musíme ho tedy nastartovat a necháme jej
// načítat třídy z adresáře, kde je umístěný bootstrap.php, a všech podadresářů (tj. __DIR__):
$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

//Podle konfiguračních souborů se generuje systémový DI kontejner, který je srdcem celé aplikace.
$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
